import {Component, ElementRef, OnInit, Renderer2, ViewChild} from '@angular/core';
import {ConfigService, XanoConfig} from "../_demo-core/config.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {MyFirstService} from "../api-services/my-first.service";
import {finalize} from "rxjs/operators";
import {MatSnackBar} from "@angular/material/snack-bar";
import {MatDialog} from "@angular/material/dialog";
import {ConfirmDeleteDialogComponent} from "../confirm-delete-dialog/confirm-delete-dialog.component";

@Component({
	selector: 'app-landing',
	templateUrl: './landing.component.html',
	styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {
	constructor(
		public configService: ConfigService,
		private router: Router,
		private route: ActivatedRoute,
		private myFirstService: MyFirstService,
		private snackBar: MatSnackBar,
		private dialog: MatDialog,
		private renderer: Renderer2
	) {
		this.config = configService.config;
	}

	public myFirstForm: FormGroup = new FormGroup({
		id: new FormControl(''),
		name: new FormControl(''),
		slogan: new FormControl(''),
		bio: new FormControl(''),
		banner_image: new FormControl(null),
		profile_image: new FormControl(null),
	});

	public config: XanoConfig;
	public data: any = [];
	public editing = false;
	public newItem: boolean;
	public singleItem: any;
	public uploading: boolean
	public profile_image: any;
	public banner_image: any;
	public bannerUpload: boolean;

	public information: any = {
		get_all: 'The data shown here was pulled from the table my_first_table using the GET /my_first_endpoint endpoint.',
		get_all_row: 'The is a row in the table my_first_table. Showing the profile_image, name, and slogan columns',
		get_single: 'This profile below was pulled from my_first_table using the GET /my_first_endpoint endpoint/{my_first_table_id}.',
		add_record: 'This will show you a view to add a new row to your table.',
		edit_record: 'This will show you a view to edit an existing row in your table.',
		delete_record: 'This will delete the record from my_first_table by calling DELETE /my_first_endpoint endpoint/{my_first_table_id}.',
		save_record: 'This will submit the new record to the table my_first_table with the form data provided by calling POST /my_first_endpoint endpoint',
		update_record: 'This will update the table my_first_table an existing record with the form data provided by calling POST /my_first_endpoint endpoint/{my_first_table_id}',
	}

	@ViewChild('xanoURL') public urlContainer: ElementRef;

	public configForm: FormGroup = new FormGroup({
		xanoUrl: new FormControl(this.configService.xanoApiUrl.value,
			[Validators.required, Validators.pattern('^https?://.+')]
		)
	})


	ngOnInit(): void {
		this.route.queryParams.subscribe(res => {
			if(res?.api_url) {
				this.configForm.controls.xanoUrl.patchValue(res.api_url)
				this.pasteSubmit();
			}
		});

		this.configService.isConfigured().subscribe(res => {
			if(res) {
				this.myFirstService.getAll().subscribe(res => {
					this.data = res;
				}, error => this.configService.showErrorSnack(error));
			}
		});
	}

	public submit(): void {
		this.configForm.markAllAsTouched();
		if (this.configForm.valid) {
			this.configService.configGet(this.configForm.controls.xanoUrl.value)
				.subscribe(res => {
						if (!this.configService.config.requiredApiPaths.every(path => res.includes(path))) {
							const message = 'This Xano Base URL is missing the required endpoints. Have you installed this marketplace extension?';
							this.configService.showErrorSnack({message: message})
						} else {
							this.configService.xanoApiUrl.next(this.configForm.controls.xanoUrl.value);
						}
					}, error => {
						console.log(error)
						this.configService.showErrorSnack(error)
					}
				);

		}
	}

	public pasteSubmit() {
		setTimeout(() => {
			this.submit();
		}, 100)
	}

	public getSingleItem(id) {
		this.myFirstService.getSingle(id).subscribe(res => {
			this.singleItem = res;
		}, error => this.configService.showErrorSnack(error))
	}

	public save() {
		if (this.singleItem?.id) {
			this.myFirstService.update(this.myFirstForm.getRawValue()).subscribe(res => {
				this.singleItem = res;
				this.editing = false;
				this.data = this.data.map(x => x.id === res.id ? res : x)

			}, error => this.configService.showErrorSnack(error))
		} else {
			this.myFirstService.save(this.myFirstForm.getRawValue()).subscribe(res => {
				this.singleItem = res;
				this.data.unshift(res);
				this.editing = false;
			}, error => this.configService.showErrorSnack(error));
		}
	}

	public viewAll() {
		this.editing = false;
		this.myFirstForm.reset();
		this.singleItem = null;
	}

	public editOrAddRecord() {
		if (!this.singleItem) {
			this.newItem = true;
		} else {
			this.myFirstForm.patchValue(this.singleItem);
		}
		this.editing = true;
	}

	public upload(event): void {
		this.uploading = true;
		const file: File = event.target.files[0];
		const formData: FormData = new FormData();
		formData.append('content', file, file.name);
		this.myFirstService.upload(formData).pipe(
			finalize(() => this.uploading = false)
		)
			.subscribe(res => {

				const reader = new FileReader();
				if (!this.bannerUpload) {
					this.myFirstForm.controls.profile_image.patchValue(res);
					reader.onload = () => this.profile_image = reader.result;
				} else {
					this.myFirstForm.controls.banner_image.patchValue(res);
					reader.onload = () => this.banner_image = reader.result;
				}

				reader.readAsDataURL(file);
			}, error => this.snackBar.open('Image Upload', 'Failed', {panelClass: 'error-snack'}));
	}

	public delete() {
		const dialogRef = this.dialog.open(ConfirmDeleteDialogComponent);

		dialogRef.afterClosed().subscribe(res => {
			if (res) {
				this.myFirstService.delete(this.singleItem?.id).subscribe(res => {
					this.data = this.data.filter(x => x.id !== this.singleItem?.id);
					this.editing = false;
					this.singleItem = null;
				}, error => this.configService.showErrorSnack(error))
			}
		})
	}

	public animateURL(): void {
		this.renderer.addClass(this.urlContainer.nativeElement, 'bounce');

		setTimeout(()=> {
			this.renderer.removeClass(this.urlContainer.nativeElement, 'bounce');
		}, 1000)
	}
}
