import {Component} from '@angular/core';
import {ConfigService} from "./_demo-core/config.service";
import {Title} from "@angular/platform-browser";
import {fadeAnimation} from "./_demo-core/router-animation";

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	animations: [fadeAnimation]
})
export class AppComponent {
	public appSummary: string = '';

	constructor(
		configService: ConfigService,
		public title: Title
	) {
		title.setTitle(configService.config.title);
		this.appSummary = configService.config.summary
	}
}
