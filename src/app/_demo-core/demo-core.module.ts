import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SafeHtmlPipe} from "./safe-html.pipe";
import {MatFormFieldModule} from "@angular/material/form-field";
import {ReactiveFormsModule} from "@angular/forms";
import {MatDividerModule} from "@angular/material/divider";
import {MatButtonModule} from "@angular/material/button";
import {MatExpansionModule} from "@angular/material/expansion";
import {MatIconModule} from "@angular/material/icon";
import {MatCardModule} from "@angular/material/card";
import {MatInputModule} from "@angular/material/input";
import {MatCommonModule} from "@angular/material/core";
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatDialogModule} from "@angular/material/dialog";

@NgModule({
	declarations: [
		SafeHtmlPipe
	],
	imports: [
		CommonModule,
		MatCommonModule,
		MatFormFieldModule,
		ReactiveFormsModule,
		MatDividerModule,
		MatButtonModule,
		MatExpansionModule,
		MatIconModule,
		MatCardModule,
		MatInputModule,
		MatTooltipModule,
		MatDialogModule
	]
})
export class DemoCoreModule {
}
