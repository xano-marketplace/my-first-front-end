import {Injectable} from '@angular/core';
import {ConfigService} from "../_demo-core/config.service";
import {ApiService} from "../_demo-core/api.service";
import {Observable} from "rxjs";

@Injectable({
	providedIn: 'root'
})
export class MyFirstService {
	constructor(
		private configService: ConfigService,
		private apiService: ApiService
	) {
	}

	public getAll(): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/my_first_endpoint`,
		});
	}

	public getSingle(id: number): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/my_first_endpoint/${id}`,
		});
	}

	public save(data: any): Observable<any> {
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/my_first_endpoint`,
			params: data,
		});
	}

	public update(data: any): Observable<any> {
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/my_first_endpoint/${data.id}`,
			params: data,
		});
	}

	public delete(id: number): Observable<any> {
		return this.apiService.delete({
			endpoint: `${this.configService.xanoApiUrl.value}/my_first_endpoint/${id}`,
		});
	}

	public upload(image): Observable<any> {
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/my_first_endpoint/upload/image`,
			params: image
		});
	}
}
